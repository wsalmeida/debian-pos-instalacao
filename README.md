# Debian-pos-instalação

Arquivos para pós instalação sistema operacional debian

# Tema de desktop
Whitesur-gtk-themes tema para desktop baseado em `GTK`.<p>
<img src="../src/imagens/mydesk.png">

# Tema de ícones
Whitesur-icon-themes tema de ícones para desktop baseados em `GTK`.<p>
<img src="../src/imagens/preview-icon.png">
<img src="../src/imagens/preview-icon-2.png">

# Fontes
Fontes usadas no sistema
- Fantasque Sans regular
- Notos Sans
